<!DOCTYPE HTML>
<html>
<head>
<script language="JavaScript" src="jquery-1.5.1.min.js"></script>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<!--<script language="JavaScript" src="jquery.watermarkinput.js"></script> -->

<script type="text/javascript">
$(document).ready(function(){

$(".form-control").keyup(function(){ //se crea la funcioin keyup
var texto = $(this).val();//se recupera el valor de la caja de texto y se guarda en la variable texto
var dataString = 'palabra='+ texto;//se guarda en una variable nueva para posteriormente pasarla a busqueda.php

if(texto==''){//si no tiene ningun valor la caja de texto no realiza ninguna accion
    //ninguna acción
}else{
//pero si tiene valor entonces
$.ajax({//metodo ajax
type: "POST",//aqui puede  ser get o post
url: "busqueda.php",//la url adonde se va a mandar la cadena a buscar
data: dataString,
cache: false,
success: function(html){//funcion que se activa al recibir un dato
$("#display").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
}
});

}
return false;
});
});

</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Recepcion</title>
</head>

<body>
 <br>

	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">Administración Recepción  </a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
			<form class="navbar-form navbar-left" role="search" action="usuario_completo.php" >
	        <div class="form-group">
	         <input type="number" name="id" class="form-control" id="caja_busqueda" placeholder="Número de habitación">
	        </div>
	        <button type="submit" class="btn btn-default">Busca Habitación</button>
	      </form>

	      </ul>

	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

<div id="display"></div>

</body>
</html>