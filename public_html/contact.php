<!--
Author: Two Crazy Developers - Jonathan Arias & Daniel B. Alzate
Author URL: https://www.facebook.com/TwoCrazyDevelopers/
-->
<?php
include 'menu.php';
 ?>
<!---->

<div class="contact-bg">
	 <div class="container">
	      <div class="contact-us">
				<div class="contact-us_left">
					<div class="contact-us_info">
			    	 	<h3 class="style">Encuentranos Aquí</h3>
			    	 		<div class="map">
					   			<iframe width="100%" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265&amp;output=embed"></iframe><br><small><a href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265" style="color: #242424;text-shadow: 0 1px 0 #ffffff;text-align: left;font-size: 0.7125em;padding: 5px;font-weight: 600;">View Larger Map</a></small>
					   		</div>
      				</div>
      			 <div class="company_address">
				     	<h3 class="style">Información del Hotel :</h3>
						<p>251500 Lorem Ipsum Dolor Sit,</p>
						<p>22-56-2-9 Sit Amet, Lorem,</p>
						<p>Colombia</p>
				   		<p>Teléfono:(00) 222 666 444</p>
				   		<p>Fax: (000) 000 00 00 0</p>
				 	 	<p>Email: <a href="mailto:info@example.com">info hotelsoft.com</a></p>
				   		<p>Síguenos en: <a href="#">Facebook</a>, <a href="#">Twitter</a></p>
				   </div>
				</div>
				<div class="contact_right">
				  <div class="contact-form">
				  	<h3 class="style">Contáctanos</h3>
					    <form method="post" action="#">
					    	<div>
						    	<span><label>NOMBRE</label></span>
						    	<span><input name="userName" type="text" class="textbox"></span>
						    </div>
						    <div>
						    	<span><label>E-MAIL</label></span>
						    	<span><input name="userEmail" type="text" class="textbox"></span>
						    </div>
						    <div>
						     	<span><label>TELÉFONO</label></span>
						    	<span><input name="userPhone" type="text" class="textbox"></span>
						    </div>
						    <div>
						    	<span><label>ASUNTO</label></span>
						    	<span><textarea name="userMsg"> </textarea></span>
						    </div>
						   <div>
						   		<input type="submit" value="Enviar">
						  </div>
					    </form>
				    </div>
  				</div>
  				<div class="clear"></div>
		  </div>
	 </div>
</div>
<!---->
<?php
include 'footer.php';
 ?>