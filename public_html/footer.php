<!--
Author: Two Crazy Developers - Jonathan Arias & Daniel B. Alzate
Author URL: https://www.facebook.com/TwoCrazyDevelopers/
-->
<!---->
<div class="fotter-info">
	  <div class="container">
	      <div class="col-md-5 details">
			 <div class="hotel-info">
				 <h4>INFORMACIÓN DEL HOTEL</h4>
				 <p>Suspendisse erat mi, tincidunt sit amet massa quis, commodo fermentum diam. Sed nec dui nec nunc tempor interdum.</p>
				 <p>Ut vulputate augue urna, ut porta dolor imperdiet a. Vestibulum nec leo eu magna aliquam ornare.</p>
			 </div>
			 <div class="news">
				 <h4>ULTIMAS NOTICIAS</h4>
				 <h5>Grand Hotel Joins DeluxelHotels</h5>
				 <a href="#">15 AUG</a>
				 <h5>Happy Chirstmas To Everyone</h5>
				 <a href="#">15 AUG</a>
				 <h5>Best Places To Visit 2014</h5>
				 <a href="#">15 AUG</a>
				 <h5>Various Offers</h5>
				 <a href="#">15 AUG</a>
			 </div>
				<div class="clearfix"></div>
		 </div>
		 <div class="col-md-7 details">
			 <div class="join">
				 <h4>HAZTE MIEMBRO</h4>
				 <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vestibulum blandit egestas.
				 Nam id lectus vel orci luctus consectetur eget id elit. In tortor odio, pellentesque eu arcu sit amet, lacinia euismod nisi. Aliquam sodales tristique mauris ac fermentum.
				 Donec vel varius ipsum. Pellentesque vitae mollis massa. </p>
				 <p>There is no costs or whatsoever so sign up today!</p>
				 <a href="#">VER MÁS</a>
			 </div>
			 <div class="member">
			 <h4>ACCESO ADMINISTRATIVO</h4>
			 <form id="form" name="form" method="post" action="../model/login.php">
					<p>Email</p>
					<input id="usuario" name="usuario" type="text" placeholder="" required/>
					<p>Contraseña</p>
					<input id="password" name="password" type="password" placeholder="" required/>
					<input name="enviar" id="enviar" type="submit" value="LOGIN"/>
			</form>
			 </div>
			 <div class="clearfix"></div>
		 </div>
		 <div class="clearfix"></div>
	  </div>
	 <h6>Creado por <a href="https://www.facebook.com/TwoCrazyDevelopers/" target="__blank">Two Crazy Developers</h6>
</div>
<!---->

</body>
</html>