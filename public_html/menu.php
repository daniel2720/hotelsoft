<!--
Author: Two Crazy Developers - Jonathan Arias & Daniel B. Alzate
Author URL: https://www.facebook.com/TwoCrazyDevelopers/
-->
<!DOCTYPE html>
<html>
<head>
<title>Hotel Soft</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Pinyon+Script' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Quicksand:400,700' rel='stylesheet' type='text/css'>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css'/>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/jquery.min.js"></script>
</head>
<body>
<!--header starts-->
<div class="header">
	 <div class="top-header">
		 <div class="container">
			 <div class="logo">
				 	<a href="index.php"><img src="images/logo.png"/></a>
			 </div>
			 <span class="menu"> </span>
			 <div class="m-clear"></div>
			 <div class="top-menu">
				<ul>
					 <li class="active"><a href="index.php">INICIO</a></li>
					 <li><a class="scroll" href="facilities.php">COMODIDADES</a></li>
					 <li><a class="scroll" href="restaurant.php">RESTAURANTE</a></li>
					 <li><a class="scroll" href="conference.php">AUDITORIOS</a></li>
					 <li><a class="scroll" href="booking.php">RESERVA</a></li>
					 <li><a class="scroll" href="contact.php">CONTACTO</a></li>
				</ul>
				<script>
					$("span.menu").click(function(){
						$(".top-menu ul").slideToggle(200);
					});
				</script>
			 </div>
			 <div class="clearfix"></div>
		  </div>
	  </div>

</div>