<!--
Author: Two Crazy Developers - Jonathan Arias & Daniel B. Alzate
Author URL: https://www.facebook.com/TwoCrazyDevelopers/
-->
<?php
include 'menu.php';
 ?>
<!---->
<div class="main_bg">
	 <div class="container">
		 <div class="main">
			   <ul class="service_list">
				 <li>
					 <div class="ser_img">
						    <a href="details.html">
							<img src="images/ser_pic1.jpg" alt="" />
							<span class="next"> </span>
							</a>
				      </div>
						<a href="details.html"><h3>Internet & WiFi</h3></a>
						<p class="para">There are many variations of passages of Lorem Ipsum available,</p>
						<h4><a  href="details.html">Lorem ipsum</a></h4>
				 </li>
				 <li>
					 <div class="ser_img">
							<a href="details.html">
							<img src="images/ser_pic2.jpg" alt="" />
							<span class="next"> </span>
							</a>
					 </div>
					 <a href="details.html"><h3>Zona Acuática Recreativa</h3></a>
					 <p class="para">There are many variations of passages of Lorem Ipsum available,</p>
					 <h4><a href="details.html">Nulla accumsan</a></h4>
				 </li>
				 <li>
					 <div class="ser_img">
							<a href="details.html">
							<img src="images/ser_pic3.jpg" alt="" />
							<span class="next"> </span>
							</a>
					 </div>
						<a href="details.html"><h3>Lavandería</h3></a>
						<p class="para">There are many variations of passages of Lorem Ipsum available,</p>
						<h4><a href="details.html">Vestilum feliing</a></h4>
				 </li>
				 <li>
					 <div class="ser_img">
							<a href="details.html">
							<img src="images/ser_pic4.jpg" alt="" />
							<span class="next"> </span>
							</a>
					 </div>
						<a href="details.html"><h3>Spa</h3></a>
						<p class="para">There are many variations of passages of Lorem Ipsum available,</p>
						<h4><a href="details.html">Nulla commodo</a></h4>
				 </li>
					<div class="clear"></div>
			  </ul>

			  <ul class="service_list top">
				 <li>
					 <div class="ser_img">
						 <a href="details.html">
						 <img src="images/ser_pic5.jpg" alt="" />
						 <span class="next"> </span>
						 </a>
					 </div>
						<a href="details.html"><h3>Los Mejores Cocineros</h3></a>
						<p class="para">There are many variations of passages of Lorem Ipsum available,</p>
						<h4><a  href="details.html">Lorem ipsum</a></h4>
				 </li>
				 <li>
					 <div class="ser_img">
					     <a href="details.html">
						 <img src="images/ser_pic6.jpg" alt="" />
						 <span class="next"> </span>
						 </a>
				     </div>
					    <a href="details.html"><h3>Bar Abierto</h3></a>
						<p class="para">There are many variations of passages of Lorem Ipsum available,</p>
						<h4><a href="details.html">Nulla accumsan</a></h4>
				 </li>
			     <li>
				     <div class="ser_img">
							<a href="details.html">
							<img src="images/ser_pic7.jpg" alt="" />
							<span class="next"> </span>
							</a>
					 </div>
						<a href="details.html"><h3>Atención a Eventos</h3></a>
						<p class="para">There are many variations of passages of Lorem Ipsum available,</p>
						<h4><a href="details.html">Vestilum feliing</a></h4>
			     </li>
				 <li>
					 <div class="ser_img">
						 <a href="details.html">
						 <img src="images/ser_pic8.jpg" alt="" />
						 <span class="next"> </span>
						 </a>
					 </div>
						<a href="details.html"><h3>Platos Especiales</h3></a>
						<p class="para">There are many variations of passages of Lorem Ipsum available,</p>
						<h4><a  href="details.html">Lorem ipsum</a></h4>
				 </li>
					<div class="clear"></div>
			  </ul>
			 <div class="clear"></div>
		   </div>
	  </div>
</div>
<!---->
<?php
include 'footer.php';
 ?>